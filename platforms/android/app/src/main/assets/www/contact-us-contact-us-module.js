(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-us-contact-us-module"],{

/***/ "+NA5":
/*!***************************************************************!*\
  !*** ./src/app/pages/contact-us/contact-us-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: ContactUsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageRoutingModule", function() { return ContactUsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _contact_us_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact-us.page */ "vtAh");




const routes = [
    {
        path: '',
        component: _contact_us_page__WEBPACK_IMPORTED_MODULE_3__["ContactUsPage"]
    }
];
let ContactUsPageRoutingModule = class ContactUsPageRoutingModule {
};
ContactUsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactUsPageRoutingModule);



/***/ }),

/***/ "I+rJ":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact-us/contact-us.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\" class=\"main-header\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      Spectrum Healthcare\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"page-container\">\r\n    <div class=\"content-header\" (click)=\"onBack()\">\r\n      <img src=\"/assets/icon/back.svg\" alt=\"back\">\r\n      <label for=\"about\">Contact Us</label>\r\n    </div>\r\n    <div class=\"page-content\">\r\n      <div class=\"contact\" *ngFor=\"let contact of contactList\">\r\n        <div class=\"city\">\r\n          <img src=\"/assets/icon/location.svg\" alt=\"location\">\r\n          <span>{{contact.city}}</span>\r\n        </div>\r\n        <p class=\"address\">{{contact.address}}</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "Pt6G":
/*!*******************************************************!*\
  !*** ./src/app/pages/contact-us/contact-us.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".contact {\n  display: flex;\n  flex-direction: column;\n  align-items: stretch;\n  box-shadow: 0 0 6px -2px #eaeaea;\n  border-radius: 4px;\n  margin-bottom: 1rem;\n  background-color: #fff;\n  flex-shrink: 0;\n}\n.contact .city {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 1rem;\n  border-bottom: 1px solid #eaeaea;\n}\n.contact .city img {\n  width: 21px;\n  margin-right: 0.5rem;\n}\n.contact .address {\n  padding: 1rem;\n  color: var(--ion-color-medium);\n  font-size: 0.9rem;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxjb250YWN0LXVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0FBQ0Y7QUFFRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGdDQUFBO0FBQUo7QUFFSTtFQUNFLFdBQUE7RUFDQSxvQkFBQTtBQUFOO0FBS0U7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxpQkFBQTtFQUNBLFNBQUE7QUFISiIsImZpbGUiOiJjb250YWN0LXVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWN0IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgYWxpZ24taXRlbXM6IHN0cmV0Y2g7XHJcbiAgYm94LXNoYWRvdzogMCAwIDZweCAtMnB4ICNlYWVhZWE7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICBmbGV4LXNocmluazogMDtcclxuXHJcblxyXG4gIC5jaXR5IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDFyZW07XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VhZWFlYTtcclxuXHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMjFweDtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwLjVyZW07XHJcbiAgICB9XHJcblxyXG4gIH1cclxuXHJcbiAgLmFkZHJlc3Mge1xyXG4gICAgcGFkZGluZzogMXJlbTtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgIGZvbnQtc2l6ZTogMC45cmVtO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "QCdY":
/*!*******************************************************!*\
  !*** ./src/app/pages/contact-us/contact-us.module.ts ***!
  \*******************************************************/
/*! exports provided: ContactUsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function() { return ContactUsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _contact_us_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact-us-routing.module */ "+NA5");
/* harmony import */ var _contact_us_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact-us.page */ "vtAh");







let ContactUsPageModule = class ContactUsPageModule {
};
ContactUsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contact_us_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactUsPageRoutingModule"]
        ],
        declarations: [_contact_us_page__WEBPACK_IMPORTED_MODULE_6__["ContactUsPage"]]
    })
], ContactUsPageModule);



/***/ }),

/***/ "vtAh":
/*!*****************************************************!*\
  !*** ./src/app/pages/contact-us/contact-us.page.ts ***!
  \*****************************************************/
/*! exports provided: ContactUsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPage", function() { return ContactUsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_contact_us_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./contact-us.page.html */ "I+rJ");
/* harmony import */ var _contact_us_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contact-us.page.scss */ "Pt6G");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");





let ContactUsPage = class ContactUsPage {
    constructor(navCtrl) {
        this.navCtrl = navCtrl;
        this.contactList = [];
    }
    ngOnInit() {
        this.contactList = [
            {
                city: 'Cooperage',
                address: `81-84, Indian Cancer Society, M. Karve Road, Cooperage, Mumbai - 400 021<br>
        +91-22-2279 0100 / 2284 6357 / 4940 / 4930`
            },
            {
                city: 'Walkeshwar',
                address: `66, Walkeshwar Road, Mumbai - 400006`
            },
            {
                city: 'Worli',
                address: `Worli, Mumbai - 400025`
            },
            {
                city: 'Parel',
                address: `Shop No. 8 & 9, Hari Bldg, Dr. Batliwala Road, Parel, Mumbai - 400012`
            },
            {
                city: 'Lower Parel',
                address: `2, Zia Masum Chawl, Opp. Deepak Talkies, N. M. Joshi Marg, Lower Parel, Mumbai - 400013`
            },
            {
                city: 'Dadar TT Circle',
                address: `44A, Doctors House, Dr. Ambedkar Road, Dadar TT Circle, Mumbai - 400014, Maharashtra`
            },
            {
                city: 'Andheri',
                address: `Suite 705, 7th Floor, Hubtown Solaris, Opp. Teli Galli, Andheri (East), Mumbai - 400069`
            },
            {
                city: 'Powai',
                address: `Shop No.10, Excel Plaza, Opp.IIT Main Gate, Powai, Mumbai - 400076, Maharashtra`
            },
            {
                city: 'Thane',
                address: `74-B/1, Brindaban Complex, Thane (W), Mumbai - 400601`
            },
            {
                city: 'Thane',
                address: `Shop No. 32, Grand Square HSG, Anand Nagar, Thane (W), Mumbai - 400615`
            },
            {
                city: 'Thane',
                address: `Office No.108, Siddhachal Complex, Near. Vasant Vihar, Thane (W), Mumbai - 400610`
            },
            {
                city: 'Sion',
                address: `Plot No. 202, Comrade Harbanslal Marg, Sion (E), Mumbai - 400022`
            },
            {
                city: 'Borivali',
                address: `A2, Royal Eksar Apartment, Chandavarkar Road, Borivali West, Mumbai 400091`
            },
            {
                city: 'Ghatkopar',
                address: `203, Trimurti Arcade, 2nd Floor, LBS Marg, Near Sarvodaya Hospital, 
        Ghatkopar (W), Mumbai - 400086`
            },
            {
                city: 'Kalyan',
                address: `Ground Floor, Jai Shivambhar Soc, Lele Ali, Tilak Chowk, Near Kalyan Gayan Samaj, 
        Old Kalyan, Kalyan (West) - 421301`
            },
            {
                city: 'Lalbaug',
                address: `012/112, Hilla Towers, Dr. S. S Rao Road, Lalbaug, Mumbai - 400012`
            },
            {
                city: 'Lower Parel',
                address: `217/1, Ambavat Bhawan, Near Janta Club, Opp. Marathon Futurex, Mafatlal Ind. N. M. Joshi Marg, 
        Mumbai - 400013`
            }
        ];
    }
    onBack() {
        this.navCtrl.navigateBack('/');
    }
};
ContactUsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] }
];
ContactUsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contact-us',
        template: _raw_loader_contact_us_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_contact_us_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ContactUsPage);



/***/ })

}]);
//# sourceMappingURL=contact-us-contact-us-module.js.map