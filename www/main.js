(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+XmF":
/*!***************************************!*\
  !*** ./src/app/guards/login.guard.ts ***!
  \***************************************/
/*! exports provided: LoginGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginGuard", function() { return LoginGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user.service */ "qfBg");




let LoginGuard = class LoginGuard {
    constructor(userService, navCtrl) {
        this.userService = userService;
        this.navCtrl = navCtrl;
    }
    canActivate(route, state) {
        const token = this.userService.getToken();
        console.log('token', token);
        if (token.value) {
            return true;
        }
        else {
            this.navCtrl.navigateRoot('/login');
            return false;
        }
    }
};
LoginGuard.ctorParameters = () => [
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
LoginGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoginGuard);



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\ionic project\spectrum\src\main.ts */"zUnb");


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    apiUrl: "http://spectrumhealthcare.in/spectrum-app-php/public/"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "O5bD":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/login/login.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"page-container bg-blue\">\r\n  <div class=\"flex-row items-center\">\r\n    <h1 class=\"app-name\">SPECTRUM</h1>\r\n  </div>\r\n  <div class=\"login-card\">\r\n    <div class=\"login-card-header\">Existing User</div>\r\n    <div class=\"card-content\">\r\n      <ion-list no-lines>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <ion-label position=\"floating\">Mobile No.</ion-label>\r\n          <ion-input [(ngModel)]=\"formData.mobile\" type=\"number\"></ion-input>\r\n        </ion-item>\r\n        <ion-item *ngIf=\"otp\" class=\"ion-no-padding\">\r\n          <ion-label position=\"floating\">OTP</ion-label>\r\n          <ion-input [(ngModel)]=\"formData.otp\" type=\"text\"></ion-input>\r\n        </ion-item>\r\n        <ion-button expand=\"full\" class=\"mt-3 login-btn\" color=\"primary\" (click)=\"login()\" *ngIf='!otp'>\r\n          Login\r\n        </ion-button>\r\n        <ion-button expand=\"full\" class=\"mt-3 login-btn\" color=\"primary\" (click)=\"verifyotp()\" *ngIf='otp'>\r\n          Verify OTP</ion-button>\r\n        <div class=\"otp-btn\" *ngIf='otp'>\r\n          <span [ngClass]=\"{'color-primary':!setDisabled}\">resend otp</span>\r\n          <label for=\"timer\" class=\"timer\" *ngIf=\"setDisabled\">\r\n            <ion-icon name=\"stopwatch-outline\" class=\"timer-icon\"></ion-icon>\r\n            00:{{timer}}\r\n          </label>\r\n        </div>\r\n      </ion-list>\r\n    </div>\r\n  </div>\r\n  <div class=\"help\">\r\n    <div class=\"help-header\">HELP</div>\r\n    <div class=\"p-3 pt-2\">\r\n      <ol class=\"help-list\">\r\n        <li>Mobile App is only for existing clients of Spectrum Healthcare.</li>\r\n        <li>Existing clients using for first time need to create account.</li>\r\n        <li>Username is your Mobile Number registered with Spectrum Healthcare.</li>\r\n        <li>For any assistance call: +91-97697-23051 / 9869201379 or email: support@spectrumhealthcare.co.in </li>\r\n      </ol>\r\n    </div>\r\n  </div>\r\n  <!--<div style=\"background-color:#fff; width:90%;margin: auto;padding: auto; border-radius: 10px; \">\r\n    <img src=\"assets/img/logo-spectrum.png\">\r\n  </div> -->\r\n</ion-content>\r\n");

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/user.service */ "qfBg");






let AppComponent = class AppComponent {
    constructor(navCtrl, userService, menu) {
        this.navCtrl = navCtrl;
        this.userService = userService;
        this.menu = menu;
        this.currentRoute = '/';
        this.subscriptions = {
            userData: null
        };
        this.subscriptions.userData = this.userService.getUser().subscribe(user => {
            if (user) {
                this.userData = user;
            }
            else {
                this.navCtrl.navigateRoot('/login');
            }
        });
    }
    navigateTo(route) {
        this.currentRoute = route;
        console.log('currentRoute', this.currentRoute);
        this.menu.close();
        switch (route) {
            case '/':
                this.navCtrl.navigateRoot('/');
                break;
            case 'about-us':
                this.navCtrl.navigateForward('/about-us');
                break;
            case 'health-report':
                this.navCtrl.navigateForward('/health-report');
                break;
            case 'take-appointment':
                this.navCtrl.navigateForward('take-appointment');
                break;
        }
    }
    onLogout() {
        this.menu.close();
        this.userService.onLogout();
        this.navCtrl.navigateRoot('/login');
    }
    ngOnDestroy() {
        for (const subscription in this.subscriptions) {
            if (this.subscriptions[subscription]) {
                this.subscriptions[subscription].unsubscribe();
            }
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AppComponent);



/***/ }),

/***/ "VuO1":
/*!***********************************************!*\
  !*** ./src/app/services/api/configuration.ts ***!
  \***********************************************/
/*! exports provided: ApiConfiguration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiConfiguration", function() { return ApiConfiguration; });
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/environments/environment */ "AytR");

class ApiConfiguration {
    constructor() {
        this.baseUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiUrl;
        this.userLoginUrl = 'user/login';
        this.resendOtpUrl = 'user/resend_otp';
        this.userInfoUrl = 'user/info';
        this.reportListUrl = 'user/getregistrationlist';
        this.testRListUrl = 'user/gettestlist';
        this.viewReportUrl = 'user/getreportresult';
        this.sendTestAppontUrl = 'user/sendTestsAppointment';
        this.sendCorAppontUrl = 'user/sendCorpAppointment';
    }
}


/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\r\n  <ion-split-pane contentId=\"main-content\">\r\n    <ion-menu contentId=\"main-content\" type=\"overlay\">\r\n      <ion-content>\r\n        <div class=\"nav\">\r\n          <div class=\"nav-header\">\r\n            <ion-icon class=\"profile\" name=\"person-circle-outline\"></ion-icon>\r\n            <div class=\"flex-column align-stretch\">\r\n              <label for=\"username\" class=\"username\">{{userData?.name}}</label>\r\n              <span>Unique ID : {{userData?.pat_id}}</span>\r\n            </div>\r\n          </div>\r\n          <div class=\"nav-list\">\r\n            <a (click)=\"navigateTo('/')\" [ngClass]=\"{'active':currentRoute=='/'}\" class=\"ion-activatable\">\r\n              <ion-icon name=\"home-outline\"></ion-icon>\r\n              <label for=\"home\">Home</label>\r\n              <ion-ripple-effect></ion-ripple-effect>\r\n            </a>\r\n            <a (click)=\"navigateTo('about-us')\" [ngClass]=\"{'active':currentRoute=='about-us'}\" class=\"ion-activatable\">\r\n              <ion-icon name=\"globe-outline\"></ion-icon>\r\n              <label for=\"Discover Spectrum\">Discover Spectrum</label>\r\n              <ion-ripple-effect></ion-ripple-effect>\r\n            </a>\r\n            <a (click)=\"navigateTo('health-report')\" [ngClass]=\"{'active':currentRoute=='health-report'}\"\r\n              class=\"ion-activatable\">\r\n              <ion-icon name=\"pulse-outline\"></ion-icon>\r\n              <label for=\"Health Report\">Health Report</label>\r\n              <ion-ripple-effect></ion-ripple-effect>\r\n            </a>\r\n            <a (click)=\"navigateTo('take-appointment')\" [ngClass]=\"{'active':currentRoute=='take-appointment'}\"\r\n              class=\"ion-activatable\">\r\n              <ion-icon name=\"today-outline\"></ion-icon>\r\n              <label for=\"Take Appointment\">Take Appointment</label>\r\n              <ion-ripple-effect></ion-ripple-effect>\r\n            </a>\r\n            <a (click)=\"onLogout()\" class=\"ion-activatable\">\r\n              <ion-icon name=\"log-out-outline\"></ion-icon>\r\n              <label for=\"Logout\">Logout</label>\r\n              <ion-ripple-effect></ion-ripple-effect>\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </ion-content>\r\n    </ion-menu>\r\n    <ion-router-outlet id=\"main-content\"></ion-router-outlet>\r\n  </ion-split-pane>\r\n</ion-app>\r\n");

/***/ }),

/***/ "W3Zi":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login.component.html */ "O5bD");
/* harmony import */ var _login_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.component.scss */ "Yz0z");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/user.service */ "qfBg");






let LoginComponent = class LoginComponent {
    constructor(navCtrl, menu, userService, loadingController) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.userService = userService;
        this.loadingController = loadingController;
        this.formData = {
            mobile: '',
            otp: ''
        };
        this.timer = 59;
        this.setDisabled = true;
    }
    ngOnInit() {
        this.startTimer();
    }
    ionViewDidEnter() {
        this.menu.close();
        this.menu.enable(false);
    }
    presentLoading(msg) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                message: msg,
            });
            yield this.loading.present();
            const { role, data } = yield this.loading.onDidDismiss();
        });
    }
    login() {
        if (this.formData.mobile) {
            this.presentLoading('Loading please wait...');
            const data = {
                password: 'sohail',
                username: '9867042277'
            };
            this.userService.onLogin(data).then((data) => {
                if (data) {
                    this.userService.saveToken(data.access_token);
                    if (data.success && data.success !== 0) {
                        this.otp = data.otp;
                        this.startTimer();
                    }
                    else {
                        console.log('nav');
                        this.navCtrl.navigateRoot('/');
                    }
                }
                this.loadingController.dismiss();
            }).catch(error => {
                console.error('Cannot loagin : ', error);
                this.loadingController.dismiss();
            });
        }
    }
    startTimer() {
        let t = this;
        let calltimer = setInterval(function () {
            if (t.timer == 0) {
                t.setDisabled = false;
            }
            else {
                t.timer = Number(t.timer) - 1;
                if (t.timer < 10) {
                    t.timer = "0" + t.timer;
                }
            }
        }, 1000);
    }
    resendOtp() {
        if (this.formData.mobile == '') {
            this.presentLoading('Please Enter mobile');
        }
        else {
            const data = {
                mobile1: this.formData.mobile,
                otp: this.formData.otp
            };
            this.userService.onResendOtp(data).then((data) => {
                this.presentLoading('Loading please wait...');
                if (data.success == 1) {
                    this.otp = data.otp;
                    this.timer = 59;
                    this.setDisabled = true;
                }
            });
            this.navCtrl.navigateRoot('/');
        }
    }
    verifyotp(credentials) {
        if (this.formData.otp == '') {
            this.presentLoading('Please Enter Otp');
        }
        else {
            if (this.formData.otp == this.otp) {
                // this.authService.verify_otp(credentials).then(data => {
                //   if (data) {
                //     //console.log("LoginData="+this.authService.loginmessage);
                //     this.presentLoading('Loading please wait...');
                //     if (this.authService.loginmessage == 0) {
                //       this.menu.enable(true, 'loggedInMenu');
                //       this.navCtrl.navigateRoot('/');
                //     } else {
                //       this.loginError();
                //     }
                //   }
                // });
                //this.navCtrl.navigateRoot('/'); 
            }
        }
    }
};
LoginComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
];
LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LoginComponent);



/***/ }),

/***/ "Yz0z":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".app-name {\n  font-family: var(--font-family-medium);\n  color: #fff;\n  font-size: 3rem;\n}\n\n.login-card {\n  background-color: #fff;\n  margin: 0.5rem;\n  border-radius: 10px;\n}\n\n.login-card .login-card-header {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n  width: 100%;\n  height: 50px;\n  color: #000;\n  font-size: 1rem;\n  border-bottom: 1px solid #eaeaea;\n  border-radius: 10px 10px 0 0;\n}\n\n.login-card .card-content {\n  display: flex;\n  flex-direction: column;\n  align-items: stretch;\n  padding: 1rem;\n}\n\n.help {\n  display: flex;\n  flex-direction: column;\n  align-items: stretch;\n  background-color: #fff;\n  border-radius: 10px;\n  margin: 1rem;\n}\n\n.help .help-header {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n  border-bottom: 1px solid #eaeaea;\n  font-family: var(--font-family-medium);\n  padding: 1rem;\n}\n\n.help .help-list {\n  font-size: 0.8rem;\n  font-style: italic;\n  color: var(--ion-color-medium);\n}\n\n.help .help-list li {\n  margin-bottom: 0.5rem;\n}\n\n.otp-btn {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding: 1rem;\n}\n\n.otp-btn span {\n  text-transform: uppercase;\n  color: var(--ion-color-medium);\n}\n\n.otp-btn .timer {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n  margin-top: 1rem;\n}\n\n.otp-btn .timer .timer-icon {\n  width: 20px;\n  height: 20px;\n  margin-right: 0.5rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNDQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFDRjs7QUFFQTtFQUNFLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0FBQ0Y7O0FBQ0U7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdDQUFBO0VBQ0EsNEJBQUE7QUFDSjs7QUFFRTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0VBQ0EsYUFBQTtBQUFKOztBQUlBO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQURGOztBQUdFO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdDQUFBO0VBQ0Esc0NBQUE7RUFDQSxhQUFBO0FBREo7O0FBSUU7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsOEJBQUE7QUFGSjs7QUFJSTtFQUNFLHFCQUFBO0FBRk47O0FBT0E7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQUpGOztBQU1FO0VBQ0UseUJBQUE7RUFDQSw4QkFBQTtBQUpKOztBQU9FO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0FBTEo7O0FBT0k7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0FBTE4iLCJmaWxlIjoibG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYXBwLW5hbWUge1xyXG4gIGZvbnQtZmFtaWx5OiB2YXIoLS1mb250LWZhbWlseS1tZWRpdW0pO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGZvbnQtc2l6ZTogM3JlbTtcclxufVxyXG5cclxuLmxvZ2luLWNhcmQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgbWFyZ2luOiAwLjVyZW07XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuXHJcbiAgLmxvZ2luLWNhcmQtaGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDAgMDtcclxuICB9XHJcblxyXG4gIC5jYXJkLWNvbnRlbnQge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogc3RyZXRjaDtcclxuICAgIHBhZGRpbmc6IDFyZW07XHJcbiAgfVxyXG59XHJcblxyXG4uaGVscCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGFsaWduLWl0ZW1zOiBzdHJldGNoO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBtYXJnaW46IDFyZW07XHJcblxyXG4gIC5oZWxwLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWFlYWVhO1xyXG4gICAgZm9udC1mYW1pbHk6IHZhcigtLWZvbnQtZmFtaWx5LW1lZGl1bSk7XHJcbiAgICBwYWRkaW5nOiAxcmVtO1xyXG4gIH1cclxuXHJcbiAgLmhlbHAtbGlzdCB7XHJcbiAgICBmb250LXNpemU6IDAuODByZW07XHJcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcblxyXG4gICAgbGkge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4ub3RwLWJ0biB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgcGFkZGluZzogMXJlbTtcclxuXHJcbiAgc3BhbiB7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xyXG4gIH1cclxuXHJcbiAgLnRpbWVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogMXJlbTtcclxuXHJcbiAgICAudGltZXItaWNvbiB7XHJcbiAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMC41cmVtO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/login/login.component */ "W3Zi");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "3Pt+");











let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _components_login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"]
        ],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
            _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["IonicStorageModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"]
        ],
        providers: [{ provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
    })
], AppModule);



/***/ }),

/***/ "f61G":
/*!**************************************************!*\
  !*** ./src/app/services/api/api-call.service.ts ***!
  \**************************************************/
/*! exports provided: ApiCallService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiCallService", function() { return ApiCallService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _configuration__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./configuration */ "VuO1");




let ApiCallService = class ApiCallService extends _configuration__WEBPACK_IMPORTED_MODULE_3__["ApiConfiguration"] {
    constructor(http) {
        super();
        this.http = http;
        this.token = '';
        const token = localStorage.getItem('authToken');
        this.token = token ? token : null;
        // console.log('token', this.token);
    }
    getHeader() {
        // this.token = `eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJlNDhhYzg0MmUzZmMwOTliM2MzZTZmNmY1OTEyYjU0ZWNhY2Y4ZTFiZmJkNmMxNjJhNWNiMzI4YjM1MGNkNmE1YTJkNmViYjExNzk3OWJhIn0.eyJhdWQiOiIyIiwianRpIjoiMmU0OGFjODQyZTNmYzA5OWIzYzNlNmY2ZjU5MTJiNTRlY2FjZjhlMWJmYmQ2YzE2MmE1Y2IzMjhiMzUwY2Q2YTVhMmQ2ZWJiMTE3OTc5YmEiLCJpYXQiOjE2MTYwODg5OTksIm5iZiI6MTYxNjA4ODk5OSwiZXhwIjoxNjQ3NjI0OTk5LCJzdWIiOiIxNSIsInNjb3BlcyI6WyIqIl19.MCbLIaAa0SGBnviUa_2q3ib-7-zs_ToXWEiybnfC7CSZgA8vpKl-lnXU1SQ9g0Vb7frbAFB0UxFQiitMXQseQjDwrQNIMP3cdM_SoQIhq7A1rmny_REGbSVCglz80hBEL1ciQiF7V8yV-mTe_C1gzu_i_xQRZEWxipEYvZXtOu65vin5jg7HiARzJsllvbLCm66e-Z0gL9nKQflmCKssFKgiBp4_wN5gzwxG250Zb9rEH1Thr3wXvnFsWP6GaZ6cEVxyQ4iFwzuo3Gl3LpSSHrL7ztYPGd8bH5kznjK_xlLogqXELo3W-kr5uEuQQJ9NsQgD10jtmiKUAdf4PHpHgSas-6hZOa-lSpag0DfLenPwL-wm1k0xpZzyPVhAGVUtj-uzR9EvSbuaskacPjwTMZdN_aVMOZVDYcoIFbnScEuosGIE32wf7Q4AcPBahTGAB8uDWVHxBuTSKamJQizX3tfqVlL0VQGuT6M7rBMqzfQMRRd4NqP7ucOvsv9P-0xI6yGnBBYTa4oItCo-qxImfrAMOvcS1efgzLqgjNeI30jrhSt9tqP4iHOXF1AcS0pBZ4kpShnPyT3qFEwtHQ4afPAon-377wROdYzG4es4XjNLKAwXRDZUuzb72xxJQqNTDQYCYWIdtyLDqTUcES_V58mA7pxDseJXcxvwwTl-SDE`;
        return {
            headers: {
                Authorization: 'Bearer ' + JSON.parse(this.token),
            }
        };
    }
    getData(subUrl, token = true) {
        return new Promise((resolve, reject) => {
            console.log('baseUrl', this.baseUrl, 'st', subUrl);
            const request = this.baseUrl + subUrl;
            console.log('value', this.getHeader());
            this.http
                .get(request, token ? this.getHeader() : {})
                .subscribe(data => resolve(data), error => reject(error));
        });
    }
    postData(subUrl, data, token = true) {
        return new Promise((resolve, reject) => {
            console.log('Token :', token);
            console.log('Data :', this.getHeader());
            const request = this.baseUrl + subUrl;
            console.log('request', request);
            this.http.post(request, data, token ? this.getHeader() : {})
                .subscribe(res => resolve(res), error => {
                console.log('Main Error :', error);
                reject(error);
            });
        });
    }
    putData(subUrl, data, token = true) {
        return new Promise((resolve, reject) => {
            const request = this.baseUrl + subUrl;
            this.http
                .put(request, data, token ? this.getHeader() : {})
                .subscribe(res => resolve(res), error => reject(error));
        });
    }
    deleteData(subUrl, token = true) {
        return new Promise((resolve, reject) => {
            const request = this.baseUrl + subUrl;
            this.http
                .delete(request, token ? this.getHeader() : {})
                .subscribe(res => resolve(res), error => reject(error));
        });
    }
};
ApiCallService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
ApiCallService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], ApiCallService);



/***/ }),

/***/ "kLfG":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet.entry.js": [
		"dUtr",
		"common",
		0
	],
	"./ion-alert.entry.js": [
		"Q8AI",
		"common",
		1
	],
	"./ion-app_8.entry.js": [
		"hgI1",
		"common",
		2
	],
	"./ion-avatar_3.entry.js": [
		"CfoV",
		"common",
		3
	],
	"./ion-back-button.entry.js": [
		"Nt02",
		"common",
		4
	],
	"./ion-backdrop.entry.js": [
		"Q2Bp",
		5
	],
	"./ion-button_2.entry.js": [
		"0Pbj",
		"common",
		6
	],
	"./ion-card_5.entry.js": [
		"ydQj",
		"common",
		7
	],
	"./ion-checkbox.entry.js": [
		"4fMi",
		"common",
		8
	],
	"./ion-chip.entry.js": [
		"czK9",
		"common",
		9
	],
	"./ion-col_3.entry.js": [
		"/CAe",
		10
	],
	"./ion-datetime_3.entry.js": [
		"WgF3",
		"common",
		11
	],
	"./ion-fab_3.entry.js": [
		"uQcF",
		"common",
		12
	],
	"./ion-img.entry.js": [
		"wHD8",
		13
	],
	"./ion-infinite-scroll_2.entry.js": [
		"2lz6",
		14
	],
	"./ion-input.entry.js": [
		"ercB",
		"common",
		15
	],
	"./ion-item-option_3.entry.js": [
		"MGMP",
		"common",
		16
	],
	"./ion-item_8.entry.js": [
		"9bur",
		"common",
		17
	],
	"./ion-loading.entry.js": [
		"cABk",
		"common",
		18
	],
	"./ion-menu_3.entry.js": [
		"kyFE",
		"common",
		19
	],
	"./ion-modal.entry.js": [
		"TvZU",
		"common",
		20
	],
	"./ion-nav_2.entry.js": [
		"vnES",
		"common",
		21
	],
	"./ion-popover.entry.js": [
		"qCuA",
		"common",
		22
	],
	"./ion-progress-bar.entry.js": [
		"0tOe",
		"common",
		23
	],
	"./ion-radio_2.entry.js": [
		"h11V",
		"common",
		24
	],
	"./ion-range.entry.js": [
		"XGij",
		"common",
		25
	],
	"./ion-refresher_2.entry.js": [
		"nYbb",
		"common",
		26
	],
	"./ion-reorder_2.entry.js": [
		"smMY",
		"common",
		27
	],
	"./ion-ripple-effect.entry.js": [
		"STjf",
		28
	],
	"./ion-route_4.entry.js": [
		"k5eQ",
		"common",
		29
	],
	"./ion-searchbar.entry.js": [
		"OR5t",
		"common",
		30
	],
	"./ion-segment_2.entry.js": [
		"fSgp",
		"common",
		31
	],
	"./ion-select_3.entry.js": [
		"lfGF",
		"common",
		32
	],
	"./ion-slide_2.entry.js": [
		"5xYT",
		33
	],
	"./ion-spinner.entry.js": [
		"nI0H",
		"common",
		34
	],
	"./ion-split-pane.entry.js": [
		"NAQR",
		35
	],
	"./ion-tab-bar_2.entry.js": [
		"knkW",
		"common",
		36
	],
	"./ion-tab_2.entry.js": [
		"TpdJ",
		"common",
		37
	],
	"./ion-text.entry.js": [
		"ISmu",
		"common",
		38
	],
	"./ion-textarea.entry.js": [
		"U7LX",
		"common",
		39
	],
	"./ion-toast.entry.js": [
		"L3sA",
		"common",
		40
	],
	"./ion-toggle.entry.js": [
		"IUOf",
		"common",
		41
	],
	"./ion-virtual-scroll.entry.js": [
		"8Mb5",
		42
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "kLfG";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "qfBg":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _api_api_call_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./api/api-call.service */ "f61G");




let UserService = class UserService {
    constructor(apiCall) {
        this.apiCall = apiCall;
        this.token = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.user = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.updateToken();
    }
    updateToken() {
        const aToken = localStorage.getItem('authToken');
        this.token.next(JSON.parse(aToken));
        const user = localStorage.getItem('user');
        this.user.next(JSON.parse(user));
    }
    saveToken(token) {
        this.token.next(token);
        localStorage.setItem('authToken', JSON.stringify(token));
    }
    saveUserData(user) {
        this.user.next(user);
        localStorage.setItem('user', JSON.stringify(user));
    }
    getUser() {
        return this.user;
    }
    getToken() {
        return this.token;
    }
    onLogin(data) {
        return this.apiCall.postData(this.apiCall.userLoginUrl, data, false);
    }
    onLogout() {
        localStorage.removeItem('authToken');
        this.token.next(null);
        localStorage.removeItem('user');
        this.user.next(null);
    }
    onResendOtp(data) {
        return this.apiCall.postData(this.apiCall.resendOtpUrl, data, false);
    }
    getUserInfo() {
        return this.apiCall.getData(this.apiCall.userInfoUrl);
    }
};
UserService.ctorParameters = () => [
    { type: _api_api_call_service__WEBPACK_IMPORTED_MODULE_3__["ApiCallService"] }
];
UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UserService);



/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/login/login.component */ "W3Zi");
/* harmony import */ var _guards_login_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./guards/login.guard */ "+XmF");





const routes = [
    {
        path: 'login',
        component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]
    },
    {
        path: '',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-dashboard-dashboard-module */ "pages-dashboard-dashboard-module").then(__webpack_require__.bind(null, /*! ./pages/dashboard/dashboard.module */ "/2RN")).then(m => m.DashboardPageModule),
        canActivate: [_guards_login_guard__WEBPACK_IMPORTED_MODULE_4__["LoginGuard"]]
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".nav {\n  display: flex;\n  flex-direction: column;\n  align-items: stretch;\n  height: 100%;\n}\n.nav .nav-header {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 1rem;\n  border-bottom: 1px solid #eaeaea;\n  flex-shrink: 0;\n  color: #fff;\n  background-color: var(--blue);\n}\n.nav .nav-header .profile {\n  width: 55px;\n  height: 55px;\n  color: var(--ion-color-medium);\n  margin-right: 1rem;\n  color: #fff;\n}\n.nav .nav-header .username {\n  font-size: 1.1rem;\n}\n.nav .nav-header span {\n  color: #d6d6d6;\n  font-size: 0.9rem;\n  margin-top: 0.2rem;\n}\n.nav .nav-list {\n  display: flex;\n  flex-direction: column;\n  align-items: stretch;\n  padding: 1rem 0;\n  flex-grow: 1;\n  overflow: auto;\n}\n.nav .nav-list a {\n  text-decoration: none;\n  outline: none;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 0.7rem 1.5rem;\n  position: relative;\n  color: var(--ion-color-medium);\n}\n.nav .nav-list a ion-icon {\n  margin-right: 1rem;\n  width: 20px;\n  height: 20px;\n}\n.nav .nav-list .active {\n  color: var(--blue) !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0FBQ0Y7QUFDRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGdDQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtBQUNKO0FBQ0k7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBQ047QUFFSTtFQUNFLGlCQUFBO0FBQU47QUFHSTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRE47QUFLRTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FBSEo7QUFLSTtFQUNFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLDhCQUFBO0FBSE47QUFLTTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFIUjtBQU9JO0VBQ0UsNkJBQUE7QUFMTiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmF2IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgYWxpZ24taXRlbXM6IHN0cmV0Y2g7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG5cclxuICAubmF2LWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAxcmVtO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWVhZWE7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tYmx1ZSk7XHJcblxyXG4gICAgLnByb2ZpbGUge1xyXG4gICAgICB3aWR0aDogNTVweDtcclxuICAgICAgaGVpZ2h0OiA1NXB4O1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMXJlbTtcclxuICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICB9XHJcblxyXG4gICAgLnVzZXJuYW1lIHtcclxuICAgICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICB9XHJcblxyXG4gICAgc3BhbiB7XHJcbiAgICAgIGNvbG9yOiAjZDZkNmQ2O1xyXG4gICAgICBmb250LXNpemU6IDAuOXJlbTtcclxuICAgICAgbWFyZ2luLXRvcDogMC4ycmVtO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLm5hdi1saXN0IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IHN0cmV0Y2g7XHJcbiAgICBwYWRkaW5nOiAxcmVtIDA7XHJcbiAgICBmbGV4LWdyb3c6IDE7XHJcbiAgICBvdmVyZmxvdzogYXV0bztcclxuXHJcbiAgICBhIHtcclxuICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBwYWRkaW5nOiAwLjdyZW0gMS41cmVtO1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuXHJcbiAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDFyZW07XHJcbiAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmFjdGl2ZSB7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1ibHVlKSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map