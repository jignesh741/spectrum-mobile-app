import { Component, OnDestroy } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnDestroy {

  currentRoute: string = '/';
  subscriptions: any = {
    userData: null
  };
  userData: any;

  constructor(
    private navCtrl: NavController,
    private userService: UserService,
    private menu: MenuController,
  ) {
    this.subscriptions.userData = this.userService.getUser().subscribe(user => {
      if (user) {
        this.userData = user;
      } else {
        this.navCtrl.navigateRoot('/login');
      }
    });
  }

  navigateTo(route) {
    this.currentRoute = route;
    console.log('currentRoute', this.currentRoute);
    this.menu.close();
    switch (route) {
      case '/':
        this.navCtrl.navigateRoot('/');
        break;
      case 'about-us':
        this.navCtrl.navigateForward('/about-us');
        break;
      case 'health-report':
        this.navCtrl.navigateForward('/health-report');
        break;
      case 'take-appointment':
        this.navCtrl.navigateForward('take-appointment');
        break;
    }

  }

  onLogout() {
    this.menu.close();
    this.userService.onLogout();
    this.navCtrl.navigateRoot('/login');
  }

  ngOnDestroy() {
    for (const subscription in this.subscriptions) {
      if (this.subscriptions[subscription]) {
        this.subscriptions[subscription].unsubscribe();
      }
    }
  }
}
