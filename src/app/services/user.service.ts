import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiCallService } from './api/api-call.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  token = new BehaviorSubject(null);
  user = new BehaviorSubject(null);

  constructor(
    private apiCall: ApiCallService
  ) {
    this.updateToken();
  }

  updateToken() {
    const aToken = localStorage.getItem('authToken');
    this.token.next(JSON.parse(aToken));
    const user = localStorage.getItem('user');
    this.user.next(JSON.parse(user));
    if (aToken) {
      this.apiCall.setHeaderToken(JSON.parse(aToken));
    }
  }

  saveToken(token) {
    this.token.next(token);
    localStorage.setItem('authToken', JSON.stringify(token));
    this.updateToken();
  }

  saveUserData(user) {
    this.user.next(user);
    localStorage.setItem('user', JSON.stringify(user));
    this.updateToken();
  }

  getUser() {
    return this.user;
  }

  getToken() {
    return this.token;
  }

  onLogin(data) {
    return this.apiCall.postData(this.apiCall.userLoginUrl, data, false);
  }

  onLogout() {
    localStorage.removeItem('authToken');
    this.token.next(null);
    localStorage.removeItem('user');
    this.user.next(null);
  }

  onResendOtp(data) {
    return this.apiCall.postData(this.apiCall.resendOtpUrl, data, false);
  }

  getUserInfo() {
    return this.apiCall.getData(this.apiCall.userInfoUrl, this.getToken().value);
  }

}
