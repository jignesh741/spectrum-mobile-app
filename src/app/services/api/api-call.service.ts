import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from '../user.service';
import { ApiConfiguration } from './configuration';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService extends ApiConfiguration {

  token: any = '';

  constructor(
    private http: HttpClient,
  ) {
    super();
    const token = localStorage.getItem('authToken');
    this.token = token ? JSON.parse(token) : null;
  }

  setHeaderToken(token) {
    this.token = token;
  }

  getHeader() {
    return {
      headers: {
        Authorization: 'Bearer ' + this.token,
      }
    };
  }

  public getData(subUrl: string, token = true): Promise<any> {
    return new Promise((resolve, reject) => {
      const request: string = this.baseUrl + subUrl;
      this.http
        .get(request,
          token ? this.getHeader() : {})
        .subscribe(
          data => resolve(data),
          error => reject(error)
        );
    });
  }

  public postData(subUrl: string, data: any, token = true): Promise<any> {
    return new Promise((resolve, reject) => {
      const request: string = this.baseUrl + subUrl;
      this.http.post(request, data, token ? this.getHeader() : {})
        .subscribe(
          res => resolve(res),
          error => {
            console.log('Main Error :', error);
            reject(error);
          }
        );
    });
  }

  public putData(subUrl: string, data: any, token = true): Promise<any> {
    return new Promise((resolve, reject) => {
      const request: string = this.baseUrl + subUrl;
      this.http
        .put(request,
          data,
          token ? this.getHeader() : {})
        .subscribe(
          res => resolve(res),
          error => reject(error)
        );
    });
  }

  public deleteData(subUrl: string, token = true): Promise<any> {
    return new Promise((resolve, reject) => {
      const request: string = this.baseUrl + subUrl;
      this.http
        .delete(request,
          token ? this.getHeader() : {})
        .subscribe(
          res => resolve(res),
          error => reject(error)
        );
    });
  }

}
