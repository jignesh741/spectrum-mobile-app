import { environment } from "src/environments/environment";

export class ApiConfiguration {

    protected baseUrl: string = environment.apiUrl;

    readonly userLoginUrl = 'user/login';
    readonly resendOtpUrl = 'user/resend_otp';
    readonly userInfoUrl = 'user/info';
    readonly reportListUrl = 'user/getregistrationlist';
    readonly testRListUrl = 'user/gettestlist';
    readonly viewReportUrl = 'user/getreportresult';
    readonly sendTestAppontUrl = 'user/sendTestsAppointment';
    readonly sendCorAppontUrl = 'user/sendCorpAppointment';
}