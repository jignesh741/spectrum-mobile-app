import { Injectable } from '@angular/core';
import { ApiCallService } from './api/api-call.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(
    private apiCall: ApiCallService
  ) { }


  getReportList(data) {
    return this.apiCall.postData(this.apiCall.reportListUrl, data);
  }

  getTestReportList(data) {
    return this.apiCall.postData(this.apiCall.testRListUrl, data);
  }

  viewReport(data) {
    return this.apiCall.postData(this.apiCall.viewReportUrl, data);
  }

  sendTestAppoint(data) {
    return this.apiCall.postData(this.apiCall.sendTestAppontUrl, data);
  }
  sendCorAppoint(data) {
    return this.apiCall.postData(this.apiCall.sendCorAppontUrl, data);
  }
}
