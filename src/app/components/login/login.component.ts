import { Component, OnInit } from '@angular/core';
import { LoadingController, MenuController, NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  formData: any = {
    mobile: '',
    otp: ''
  };
  timer: any = 59;
  setDisabled = true;
  public logresponse: any;
  otp: any;
  loading: any;

  constructor(
    public navCtrl: NavController,
    public menu: MenuController,
    public userService: UserService,
    public loadingController: LoadingController
  ) {
    localStorage.removeItem('authToken');
  }

  ngOnInit() {
    this.startTimer();
  }

  ionViewDidEnter() {
    this.menu.close();
    this.menu.enable(false);
  }

  async presentLoading(msg) {
    this.loading = await this.loadingController.create({
      message: msg,
    });
    await this.loading.present();

    const { role, data } = await this.loading.onDidDismiss();
  }

  login() {
    if (this.formData.mobile) {
      this.presentLoading('Loading please wait...');
      const data = {
        password: 'sohail',
        username: '9867042277'
      };
      this.userService.onLogin(data).then((data: any) => {
        if (data) {
          this.userService.saveToken(data.access_token);
          if (data.success && data.success !== 0) {
            this.otp = data.otp;
            this.startTimer();
          } else {
            console.log('nav');
            this.navCtrl.navigateRoot('/');
          }
        }
        this.loadingController.dismiss();
      }).catch(error => {
        console.error('Cannot loagin : ', error);
        this.loadingController.dismiss();
      });
    }
  }

  startTimer() {
    let t = this;
    let calltimer = setInterval(function () {
      if (t.timer == 0) {
        t.setDisabled = false;
      } else {
        t.timer = Number(t.timer) - 1;
        if (t.timer < 10) {
          t.timer = "0" + t.timer;
        }
      }
    }, 1000);
  }


  resendOtp() {
    if (this.formData.mobile == '') {
      this.presentLoading('Please Enter mobile')
    } else {
      const data = {
        mobile1: this.formData.mobile,
        otp: this.formData.otp
      }
      this.userService.onResendOtp(data).then((data: any) => {
        this.presentLoading('Loading please wait...');
        if (data.success == 1) {
          this.otp = data.otp;
          this.timer = 59;
          this.setDisabled = true;
        }
      });
      this.navCtrl.navigateRoot('/');
    }
  }

  verifyotp(credentials) {
    if (this.formData.otp == '') {
      this.presentLoading('Please Enter Otp')
    } else {
      if (this.formData.otp == this.otp) {
        // this.authService.verify_otp(credentials).then(data => {
        //   if (data) {
        //     //console.log("LoginData="+this.authService.loginmessage);
        //     this.presentLoading('Loading please wait...');
        //     if (this.authService.loginmessage == 0) {
        //       this.menu.enable(true, 'loggedInMenu');
        //       this.navCtrl.navigateRoot('/');
        //     } else {
        //       this.loginError();
        //     }
        //   }
        // });
        //this.navCtrl.navigateRoot('/'); 
      }
    }
  }



}
