import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HealthReportPageRoutingModule } from './health-report-routing.module';

import { HealthReportPage } from './health-report.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HealthReportPageRoutingModule
  ],
  declarations: [HealthReportPage]
})
export class HealthReportPageModule {}
