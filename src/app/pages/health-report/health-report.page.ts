import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { ReportService } from 'src/app/services/report.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-health-report',
  templateUrl: './health-report.page.html',
  styleUrls: ['./health-report.page.scss'],
})
export class HealthReportPage implements OnInit, OnDestroy {

  loading: boolean = false;
  reportList: any[] = [];
  subscriptions: any = {
    userData: null
  };
  userData: any;

  constructor(
    private navCtrl: NavController,
    private reportService: ReportService,
    public loadingController: LoadingController,
    private userService: UserService
  ) {
    this.subscriptions.userData = this.userService.getUser().subscribe(user => {
      if (user) {
        this.userData = user;
        console.log('user', user);
      }
    });
  }

  ngOnInit() {
    this.getReportList();
  }

  async presentLoading(msg) {
    let loading = await this.loadingController.create({
      message: msg,
    });
    await loading.present();
  }

  onBack() {
    this.navCtrl.navigateBack('/');
  }

  getReportList() {
    this.loading = true;
    this.presentLoading('Please Wait...');
    const data = {
      userId: this.userData.pat_id
    }
    this.reportService.getReportList(data).then(res => {
      if (res) {
        console.log('get report list : ', res);
        if (res.registrations && res.registrations.length && res.registrationsold && res.registrationsold.length) {
          this.reportList = res.registrations.concat(res.registrationsold);
        } else if (res.registrations && res.registrations.length) {
          this.reportList = res.registrations;
        } else if (res.registrationsold && res.registrationsold.length) {
          this.reportList = res.registrationsold;
        }
      }
      this.loadingController.dismiss();
      this.loading = false;
    }).catch(error => {
      this.loadingController.dismiss();
      this.loading = false;
      console.error('Cannot get reports : ', error);
    });
  }

  viewTest(id) {
    this.navCtrl.navigateForward(['/health-report/test-report', { id: id }])
  }

  ngOnDestroy() {
    for (const subscription in this.subscriptions) {
      if (this.subscriptions[subscription]) {
        this.subscriptions[subscription].unsubscribe();
      }
    }
  }
}
