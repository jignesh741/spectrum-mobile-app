import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { ReportService } from 'src/app/services/report.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-send-appointment',
  templateUrl: './send-appointment.page.html',
  styleUrls: ['./send-appointment.page.scss'],
})
export class SendAppointmentPage implements OnInit, OnDestroy {

  appointmentForm: FormGroup;
  loading: boolean = false;
  isSubmit: boolean = false;
  subscriptions: any = {
    userData: null,
    userType: null
  };
  userData: any;
  userType: any;
  constructor(
    private navCtrl: NavController,
    private userService: UserService,
    private reportService: ReportService,
    private route: ActivatedRoute,
    public alertController: AlertController,
    public loadingController: LoadingController,
  ) {
    this.subscriptions.userType = this.route.params.subscribe(type => {
      if (type) {
        this.userType = type.userType;
      } else {
        this.navCtrl.navigateRoot('/');
      }
    })
    this.subscriptions.userData = this.userService.getUser().subscribe(user => {
      if (user) {
        this.userData = user;
      } else {
        this.navCtrl.navigateRoot('/login');
      }
    });
  }

  ngOnInit() {
    this.initForm();
  }

  // convenience getter for easy access to form fields
  get f() { return this.appointmentForm.controls; }

  initForm() {
    this.appointmentForm = new FormGroup({
      radiology: new FormControl('', [Validators.required]),
      pathology: new FormControl('', [Validators.required]),
      cardiology: new FormControl('', [Validators.required]),
      other: new FormControl(''),
    });
  }

  async presentLoading(msg) {
    let load = await this.loadingController.create({
      message: msg,
    });
    await load.present();
  }

  sendAppointment() {
    this.isSubmit = true;
    console.log('this.appointmentForm.valid', this.appointmentForm.valid);

    if (this.appointmentForm.valid && this.userData && this.userType) {
      this.loading = true;
      this.presentLoading('Please wait...');
      const data = {
        pat_id: this.userData.pat_id,
        usertype: this.userType,
        Tests: {
          Radiology: this.appointmentForm.get('radiology').value,
          Pathology: this.appointmentForm.get('pathology').value,
          Cardiology: this.appointmentForm.get('cardiology').value,
          Other: this.appointmentForm.get('other').value,
        }
      };
      this.reportService.sendTestAppoint(data).then(res => {
        if (res) {
          console.log('send appointment : ', res);
          this.showThanksMsg();
        }
        this.loading = false;
        this.loadingController.dismiss();
      }).catch(error => {
        this.loading = false;
        this.loadingController.dismiss();
        console.error('Cannot send appointment : ', error);
      });

    }
  }

  async showThanksMsg() {
    const alert = await this.alertController.create({
      cssClass: 'success-msg',
      header: 'Success',
      message: 'Your appointment request has been sent to administrator. You will receive call in 24 hours.',
      buttons: [{
        text: 'Close',
        role: 'cancel',
        cssClass: 'primary',
        handler: () => {
          this.navCtrl.navigateRoot('/');
        }
      },]
    });

    await alert.present();
  }

  onBack() {
    this.navCtrl.navigateBack('/take-appointment');
  }

  ngOnDestroy() {
    for (const subscription in this.subscriptions) {
      if (this.subscriptions[subscription]) {
        this.subscriptions[subscription].unsubscribe();
      }
    }
  }

}
