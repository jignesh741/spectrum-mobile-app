import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendAppointmentPage } from './send-appointment.page';

const routes: Routes = [
  {
    path: '',
    component: SendAppointmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SendAppointmentPageRoutingModule {}
