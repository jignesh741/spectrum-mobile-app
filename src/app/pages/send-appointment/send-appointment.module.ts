import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendAppointmentPageRoutingModule } from './send-appointment-routing.module';

import { SendAppointmentPage } from './send-appointment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SendAppointmentPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SendAppointmentPage]
})
export class SendAppointmentPageModule {}
