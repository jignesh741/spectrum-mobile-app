import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'app-view-report',
  templateUrl: './view-report.page.html',
  styleUrls: ['./view-report.page.scss'],
})
export class ViewReportPage implements OnInit {

  loading: boolean = false;
  reportDetails: any;
  subscriptions: any = {
    id: null
  };
  id: any;
  pageId: any;

  graphes: any[] = [];
  centerId: any = '';
  resultId: any = '';

  constructor(
    private navCtrl: NavController,
    private reportService: ReportService,
    private route: ActivatedRoute,
    public loadingController: LoadingController,
  ) {
    this.subscriptions.id = this.route.params.subscribe(data => {
      if (data) {
        this.id = data.id;
        this.pageId = data.pageId;
      } else {
        this.onBack();
      }
    });
  }

  ngOnInit() {
    this.getReportDetails();
  }

  async presentLoading(msg) {
    let loading = await this.loadingController.create({
      message: msg,
    });
    await loading.present();
  }

  getReportDetails() {
    console.log(this.id);

    if (this.id) {
      this.loading = true;
      this.presentLoading('Please Wait...');
      const data = {
        resultId: this.id
      };
      this.reportService.viewReport(data).then(res => {
        if (res) {
          this.reportDetails = res;
          if (this.reportDetails.testingResultNew && this.reportDetails.testingResultNew.length) {
            this.centerId = this.reportDetails.testingResultNew[0].center_id;
            this.resultId = this.reportDetails.testingResultNew[0].id;
          } else if (this.reportDetails.testingResultOLD && this.reportDetails.testingResultOLD.length) {
            this.centerId = this.reportDetails.testingResultOLD[0].center_id;
            this.resultId = this.reportDetails.testingResultOLD[0].id;
          }
        }
        console.log('Get report details : ', res);
        this.loading = false;
        this.loadingController.dismiss();
      }).catch(error => {
        this.loading = false;
        this.loadingController.dismiss();
        console.error('Cannot get report details : ', error);
      });
    }
  }

  onDownload(graph) {
    if (this.centerId && this.resultId && graph) {
      let link = 'http://spectrumhealthcare.in/images/resultimages/' + this.centerId + '/' + this.resultId + '/' + graph;
      window.open(link);
    }
  }

  onBack() {
    this.navCtrl.navigateBack(['/health-report/test-report', { id: this.pageId }]);
  }

  ngOnDestroy() {
    for (const subscription in this.subscriptions) {
      if (this.subscriptions[subscription]) {
        this.subscriptions[subscription].unsubscribe();
      }
    }
  }

}
