import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppointmentPlanPage } from './appointment-plan.page';

const routes: Routes = [
  {
    path: '',
    component: AppointmentPlanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppointmentPlanPageRoutingModule {}
