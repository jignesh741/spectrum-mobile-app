import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { ReportService } from 'src/app/services/report.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-appointment-plan',
  templateUrl: './appointment-plan.page.html',
  styleUrls: ['./appointment-plan.page.scss'],
})
export class AppointmentPlanPage implements OnInit, OnDestroy {

  planList: any[] = [];
  subscriptions: any = {
    userData: null,
    userType: null
  };
  userData: any;
  userType: any;

  constructor(
    private navCtrl: NavController,
    private userService: UserService,
    private reportService: ReportService,
    private route: ActivatedRoute,
    public alertController: AlertController,
    public loadingController: LoadingController,
  ) {
    this.subscriptions.userType = this.route.params.subscribe(type => {
      if (type) {
        this.userType = type.userType;
      } else {
        this.onBack();
      }
    });

    this.subscriptions.userData = this.userService.getUser().subscribe(user => {
      if (user) {
        this.userData = user;
      } else {
        this.navCtrl.navigateRoot('/login');
      }
    });
  }

  ngOnInit() {
    this.planList = [
      {
        title: 'CEO Plus Health Package Plan',
        description: 'CEO Plan + Cancer Markers + Quarterly Risk Assessment & Online Consultations for One Year'
      },
      {
        title: 'CEO HEALTH PACKAGE PLAN',
        description: 'Master Plan + Vitamin D + B12 + Quarterly Risk Bone Densitometry Assessment & Online Consultations for One Year.'
      },
      {
        title: 'Master Plan',
        description: 'Comprehensive Health Checkup Plan with Cardiac Risk Markers, CA-125, PSA, Thyroid Profile'
      },
      {
        title: 'Comprehensive Health Checkup Plan A (Age Above 45 yrs.)',
        description: 'With Sonography, Stress Test & 2D Echo Cardiogram'
      },
      {
        title: 'Executive Health Checkup Plan B (Age 35-45 yrs.)',
        description: 'With Stress Test or 2D Echo Cardiogram'
      },
      {
        title: 'Standard Health Checkup Plan - C (Age below 35 yrs.)',
        description: 'With Special Test Serum Calcium'
      }
    ];
  }

  async presentLoading(msg) {
    let load = await this.loadingController.create({
      message: msg,
    });
    await load.present();
  }

  sendAppointment(plan) {
    if (plan && this.userData && this.userType) {
      this.presentLoading('Please wait...')
      const data = {
        Plan: plan,
        pat_id: this.userData.pat_id,
        usertype: this.userType
      };
      this.reportService.sendCorAppoint(data).then(res => {
        if (res) {
          console.log('Send appointment : ', res);
          this.showThanksMsg();
        }
        this.loadingController.dismiss();
      }).catch(error => {
        this.loadingController.dismiss();
        console.error('Cannot send appointment : ', error);
      });
    }
  }

  async showThanksMsg() {
    const alert = await this.alertController.create({
      cssClass: 'success-msg',
      header: 'Success',
      message: 'Your appointment request has been sent to administrator. You will receive call in 24 hours.',
      buttons: [{
        text: 'Close',
        role: 'cancel',
        cssClass: 'primary',
        handler: () => {
          this.navCtrl.navigateRoot('/');
        }
      },]
    });

    await alert.present();
  }

  onBack() {
    this.navCtrl.navigateBack('/take-appointment');
  }

  ngOnDestroy() {
    for (const subscription in this.subscriptions) {
      if (this.subscriptions[subscription]) {
        this.subscriptions[subscription].unsubscribe();
      }
    }
  }

}
