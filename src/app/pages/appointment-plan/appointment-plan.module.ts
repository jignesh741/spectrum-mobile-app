import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AppointmentPlanPageRoutingModule } from './appointment-plan-routing.module';

import { AppointmentPlanPage } from './appointment-plan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppointmentPlanPageRoutingModule
  ],
  declarations: [AppointmentPlanPage]
})
export class AppointmentPlanPageModule {}
