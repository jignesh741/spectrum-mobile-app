import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  constructor(
    private menu: MenuController,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getUserInfo();
  }

  ionViewDidEnter() {
    this.menu.enable(true);
  }

  getUserInfo() {
    this.userService.getUserInfo().then(res => {
      console.log('get user info : ', res);
      this.userService.saveUserData(res);
    }).catch(error => {
      console.error('Cannot get user info : ', error);
    });
  }

}
