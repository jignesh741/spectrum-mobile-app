import { Component, OnDestroy } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnDestroy {

  subscriptions: any = {
    userData: null
  }
  userData: any;

  constructor(
    private userService: UserService,
    private navCtrl: NavController
  ) {
    this.subscriptions.userData = this.userService.getUser().subscribe(user => {
      if (user) {
        this.userData = user;
      } else {
        this.navCtrl.navigateRoot('/login');
      }
    })
  }

  ngOnDestroy() {
    for (const subscription in this.subscriptions) {
      if (this.subscriptions[subscription]) {
        this.subscriptions[subscription].unsubscribe();
      }
    }
  }

}
