import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'app-test-report',
  templateUrl: './test-report.page.html',
  styleUrls: ['./test-report.page.scss'],
})
export class TestReportPage implements OnInit, OnDestroy {

  loading: boolean = false;
  reportList: any[] = [];
  subscriptions: any = {
    rId: null,
  };
  rId: any;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private reportService: ReportService,
    public loadingController: LoadingController,
  ) {
    this.subscriptions.rId = this.route.params.subscribe(id => {
      if (id) {
        this.rId = id.id;
      } else {
        this.onBack();
      }
    })
  }

  ngOnInit() {
    this.getTestReport();
  }


  async presentLoading(msg) {
    let loading = await this.loadingController.create({
      message: msg,
    });
    await loading.present();
  }

  getTestReport() {
    if (this.rId) {
      this.loading = true;
      this.presentLoading('Please Wait...');
      const data = {
        regId: this.rId
      }
      this.reportService.getTestReportList(data).then(res => {
        if (res && res.finalresult) {
          const keys = Object.keys(res.finalresult);
          if (keys && keys.length) {
            for (const key of keys) {
              this.reportList.push({
                department: key,
                reports: res.finalresult[key]
              });
            }
          }
        }
        console.log('Get test report list : ', res);
        this.loading = false;
        this.loadingController.dismiss();
      }).catch(error => {
        console.error('Cannot get test report list : ', error);
        this.loading = false;
        this.loadingController.dismiss();
      });
    }
  }

  viewReport(id) {
    console.log('this.rid', this.rId);

    const data = {
      apiId: id,
      pageId: this.rId
    }
    this.navCtrl.navigateForward(['/health-report/test-report/view-report', { id: id, pageId: this.rId }])
  }

  onBack() {
    this.navCtrl.navigateBack('/health-report');
  }

  ngOnDestroy() {
    for (const subscription in this.subscriptions) {
      if (this.subscriptions[subscription]) {
        this.subscriptions[subscription].unsubscribe();
      }
    }
  }

}
