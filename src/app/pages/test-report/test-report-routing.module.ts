import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestReportPage } from './test-report.page';

const routes: Routes = [
  {
    path: '',
    component: TestReportPage
  },
  {
    path: 'view-report',
    loadChildren: () => import('../view-report/view-report.module').then(m => m.ViewReportPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestReportPageRoutingModule { }
