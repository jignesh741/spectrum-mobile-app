import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestReportPageRoutingModule } from './test-report-routing.module';

import { TestReportPage } from './test-report.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestReportPageRoutingModule
  ],
  declarations: [TestReportPage]
})
export class TestReportPageModule {}
