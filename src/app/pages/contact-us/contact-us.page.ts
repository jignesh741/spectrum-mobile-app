import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.page.html',
  styleUrls: ['./contact-us.page.scss'],
})
export class ContactUsPage implements OnInit {

  contactList: any = [];

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    this.contactList = [
      {
        city: 'Cooperage',
        address: `81-84, Indian Cancer Society, M. Karve Road, Cooperage, Mumbai - 400 021<br>
        +91-22-2279 0100 / 2284 6357 / 4940 / 4930`
      },
      {
        city: 'Walkeshwar',
        address: `66, Walkeshwar Road, Mumbai - 400006`
      },
      {
        city: 'Worli',
        address: `Worli, Mumbai - 400025`
      },
      {
        city: 'Parel',
        address: `Shop No. 8 & 9, Hari Bldg, Dr. Batliwala Road, Parel, Mumbai - 400012`
      },
      {
        city: 'Lower Parel',
        address: `2, Zia Masum Chawl, Opp. Deepak Talkies, N. M. Joshi Marg, Lower Parel, Mumbai - 400013`
      },
      {
        city: 'Dadar TT Circle',
        address: `44A, Doctors House, Dr. Ambedkar Road, Dadar TT Circle, Mumbai - 400014, Maharashtra`
      },
      {
        city: 'Andheri',
        address: `Suite 705, 7th Floor, Hubtown Solaris, Opp. Teli Galli, Andheri (East), Mumbai - 400069`
      },
      {
        city: 'Powai',
        address: `Shop No.10, Excel Plaza, Opp.IIT Main Gate, Powai, Mumbai - 400076, Maharashtra`
      },
      {
        city: 'Thane',
        address: `74-B/1, Brindaban Complex, Thane (W), Mumbai - 400601`
      },
      {
        city: 'Thane',
        address: `Shop No. 32, Grand Square HSG, Anand Nagar, Thane (W), Mumbai - 400615`
      },
      {
        city: 'Thane',
        address: `Office No.108, Siddhachal Complex, Near. Vasant Vihar, Thane (W), Mumbai - 400610`
      },
      {
        city: 'Sion',
        address: `Plot No. 202, Comrade Harbanslal Marg, Sion (E), Mumbai - 400022`
      },
      {
        city: 'Borivali',
        address: `A2, Royal Eksar Apartment, Chandavarkar Road, Borivali West, Mumbai 400091`
      },
      {
        city: 'Ghatkopar',
        address: `203, Trimurti Arcade, 2nd Floor, LBS Marg, Near Sarvodaya Hospital, 
        Ghatkopar (W), Mumbai - 400086`
      },
      {
        city: 'Kalyan',
        address: `Ground Floor, Jai Shivambhar Soc, Lele Ali, Tilak Chowk, Near Kalyan Gayan Samaj, 
        Old Kalyan, Kalyan (West) - 421301`
      },
      {
        city: 'Lalbaug',
        address: `012/112, Hilla Towers, Dr. S. S Rao Road, Lalbaug, Mumbai - 400012`
      },
      {
        city: 'Lower Parel',
        address: `217/1, Ambavat Bhawan, Near Janta Club, Opp. Marathon Futurex, Mafatlal Ind. N. M. Joshi Marg, 
        Mumbai - 400013`
      }
    ];
  }

  onBack() {
    this.navCtrl.navigateBack('/');
  }

}
