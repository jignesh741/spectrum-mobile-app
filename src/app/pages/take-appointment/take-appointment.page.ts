import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-take-appointment',
  templateUrl: './take-appointment.page.html',
  styleUrls: ['./take-appointment.page.scss'],
})
export class TakeAppointmentPage implements OnInit, OnDestroy {

  selectedUserType: any;

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.selectedUserType = '';
  }

  onSelectUserType(type) {
    this.selectedUserType = type;
  }

  onSelectCheckupType(type) {
    switch (type) {
      case 'healthCheckup':
        this.navCtrl.navigateForward(['/take-appointment/appointment-plan', { userType: this.selectedUserType }]);
        break;
      case 'individualTest':
        this.navCtrl.navigateForward(['/take-appointment/send-appointment', { userType: this.selectedUserType }]);
        break;
    }
  }

  onBack() {
    this.navCtrl.navigateBack('/');
    this.selectedUserType = '';
  }


  ngOnDestroy() {
    this.selectedUserType = '';
  }

}
