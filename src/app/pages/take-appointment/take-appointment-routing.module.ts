import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TakeAppointmentPage } from './take-appointment.page';

const routes: Routes = [
  {
    path: '',
    component: TakeAppointmentPage
  },
  {
    path: 'appointment-plan',
    loadChildren: () => import('../appointment-plan/appointment-plan.module').then(m => m.AppointmentPlanPageModule)
  },
  {
    path: 'send-appointment',
    loadChildren: () => import('../send-appointment/send-appointment.module').then(m => m.SendAppointmentPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TakeAppointmentPageRoutingModule { }
