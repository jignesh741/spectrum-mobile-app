import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TakeAppointmentPageRoutingModule } from './take-appointment-routing.module';

import { TakeAppointmentPage } from './take-appointment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TakeAppointmentPageRoutingModule
  ],
  declarations: [TakeAppointmentPage]
})
export class TakeAppointmentPageModule {}
